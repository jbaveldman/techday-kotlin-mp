package app.todoList

import app.common.models.TodoItem
import app.common.models.TodoList
import app.logging.Logging
import app.todoList.service.TodoListService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.html.DIV
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.RDOMBuilder
import react.dom.button
import react.dom.div
import react.dom.table
import react.dom.tbody
import react.dom.td
import react.dom.th
import react.dom.tr
import react.setState
import kotlin.browser.window


interface TectonicProps : RProps {
    var todoListId: Int
}

interface TectonicState : RState {
    var todoList: TodoList
    var feedback: String
    var buttonColor: String?
}

class TodoListDisplay(props: TectonicProps) : RComponent<TectonicProps, TectonicState>(props) {

    private val todoListService = TodoListService()
    private val logger = Logging()

    override fun TectonicState.init(props: TectonicProps) {
        todoList = initToDo()

        GlobalScope.launch {
            logger.log("Trying to replace todolist")

            val newTodoList = todoListService.getTodoList()
            todoList = newTodoList
            setState {
                todoList = newTodoList
            }
        }
    }

    override fun RBuilder.render() {
        div("todoList") {

            //IntelliJ thinks items is allways set, but JS apparently doesnt set an empty array, so this check is necessary
            if (state.todoList.items != null && state.todoList.items.isNotEmpty()) {
                table("striped--light-silver:nth-child(odd)") {

                    tbody {
                        tr{
                            th{ +state.todoList.name }
                        }
                        state.todoList.items.sortedBy { it.priority }.forEach {
                            tr {
                                td {
                                    +it.description
                                }
                            }
                        }
                    }
                }
            } else {
                +"No items found"
            }
        }

        div("feedback") {
            feedbackButton()
            //You cant allways trust Intellij feedback in KotlinJS, the 'unused' elvis operator is actually used
            div("feedback") {
                +"Feedback: ${state.feedback ?: ""} "
            }
        }
    }

    private fun initToDo(): TodoList {
        val item = TodoItem("Run the backend application", 2)
        val item2 = TodoItem("Get yourself a drink", 1)
        return TodoList("Techday todo list placeholder", arrayOf(item, item2))
    }

    private fun RDOMBuilder<DIV>.feedbackButton() {
        button(classes = "button feedback") {
            +"Get feedback"
            attrs {
                onClickFunction = {
                    setFeedback("Good looking list")
                    setFeedbackButtonColor("blue")
                }
            }
        }
    }

    private fun setFeedbackButtonColor(string: String) {
        setState {
            buttonColor = string
        }
        window.setTimeout({
            setState { buttonColor = "" }
        }, 3000)
    }

    fun setFeedback(feedbackString: String) {
        setState {
            feedback = feedbackString
        }
        window.setTimeout({
            setState { feedback = "" }
        }, 3000)
    }
}

fun RBuilder.todoList(todoListId: Int) = child(TodoListDisplay::class) {
    attrs.todoListId = todoListId
}
