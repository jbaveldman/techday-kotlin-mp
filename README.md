Techday multiplatform readme

Working project:
    * First we need a working gradle build:
        * Edit the file: 'main/local.properties'. Edit the location to your android sdk to f.e.: sdk.dir=/Users/USERNAME/Library/Android/sdk
        * If your not using android, alternatively you can exclude the android folder from the settings.gradle 
    * Make sure you can run the projects below

Web-react 
    Running: 
        * Execute gradle task: web-react/webpack/webpack-run 
        * Open localhost
        * Doenst work? Something is probably allready running on port 8088
    Notes:
        * Webpack used the build folder, which is not allways up to date. Try running build/clean when encountering strange errors 
        * Is a text not displayed as you expect? The + is probably missing: +"Something" 
   Known issues:
        * Functions of objects retreived from the backend might not be known in JS. Use extension functions to circumvent this problemn
        * Lists are not well send over the line. Use arrays instead 

Backend
    Running: 
        * Spring boot application: Run main.kt in intelliJ 
        * When running the application, the frontend should show a differt example todo list 
    Notes:
        * The backend uses Kotlin with Spring Boot, a good combination. https://www.tutorialspoint.com/spring_boot/spring_boot_introduction.htm 
        * A dependency for mongoDB is included. Other databases can be used too  

Android
    Running:
        * Open the main folder in Android Studio (not the android folder!)
        * After indexing run MainActivity, choose a (virtual) machine to run on 
    Notes:
        * This is a normal android application, apart from using the common module
        * Opening the project both intelliJ and Android studio at the same time might cause problems. Refreshing gradle or closing the other application might help 
  

Common
    Notes:
    * There is quite a complicated common structure; common <- common-js <- common-client-js <- web-react for example. I couldn't strip common-client-js unfortunately.
    * Has an example model that is used in backend, android and web-react. 
    * Has an example expected class. Actual classes can be found in common-jvm and common-js
